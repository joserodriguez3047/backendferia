require('./config/conexion');

const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const port = (process.env.port || '3000');
const app = express();
const server = require('http').Server(app);
const conexion = require('./config/conexion')
const io = require('socket.io')(server,{
    cors: {
        origin: true,
        credentials: true,
        methods: ["GET", "POST", "PUT"]
    }
});

io.on('connection', (socket) =>{
    socket.on("editProduct", ()=>{
        socket.broadcast.emit("show");
    })

    socket.on("deleteProduct", ()=>{
        socket.broadcast.emit("showorder");
    })
})

app.use(function (req, res, next) {
    // Website you wish to allow to connect
    //res.setHeader('Access-Control-Allow-Origin', 'http://localhost:4200');
    res.setHeader('Access-Control-Allow-Origin', 'https://www.eventoscambiagro.com');
    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');
    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
    // Set to true if you need the website to include cookies in the requests sent
    // to the API (e.g. in case you use sessions)
    res.setHeader('Access-Control-Allow-Credentials', true);
    // Pass to next layer of middleware
    next();
});

app.use(express.json());

//config
app.set('port', port);


app.use('/api', require('./routes'));

server.listen(app.get('port'), (err) => {
    if(err){
        console.log('error al iniciar el server ' + err);
    }else{
        console.log('servidor iniciado: ' + port);
    }
})
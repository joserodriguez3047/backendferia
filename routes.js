const route = require('express').Router();
const conexion = require('./config/conexion')
const nodemailer = require("nodemailer")

route.get('/', function(req, res, next) {
    let sql = 'select * from catalog_product_entity'
    conexion.query(sql, (err, rows, fields) =>{
        
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.get('/oferta/:id', function(req, res, next) {
    console.log("view ofertas");
    const {id} = req.params
    let sql = `select o.entity_id, o.tipo, o.ofertasap, c.usr, o.orden_id, o.quantity as qty, case when numref is not null then '-' when (p.qty - (select sum(quantity) from oferta where orden_id = o.orden_id and numref is not null)) is null then o.quantity else (p.qty - (select sum(quantity) from oferta where orden_id = o.orden_id and numref is not null)) end as disponible, cpe.name, p.price, (p.price*o.quantity) as total, estado, o.tipopedido, o.numref, o.bodega, cpe.sector, c.customer_group_code, c.destinatarios from oferta o join orders p on o.orden_id = p.entity_id join customer c on o.customer_group_code = c.customer_group_code join catalog_product_entity cpe on cpe.entity_id = p.product_id where c.usr = '${id}'`
    conexion.query(sql, (err, rows, fields) =>{
        
        if(err) throw err;
        else{
            console.log(rows);
            res.json(rows);
        }
    })
});

route.get('/destinatarios/:id', function(req, res, next) {
    console.log("view destinatarios");
    const {id} = req.params
    let sql = `select texto, destinatario from destinatarios where usr = '${id}'`
    conexion.query(sql, (err, rows, fields) =>{
        
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.get('/clientesferia', function(req, res, next) {
    const {id} = req.params
    let sql = `select usr as 'codigosap', company_email from customer`
    conexion.query(sql, (err, rows, fields) =>{
        
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.get('/pedidos', function(req, res, next) {
    let sql = `select c.name as name, sum(qty) as qty, sum(total) as total from orders as o join catalog_product_entity as c on c.entity_id = o.product_id  group by c.name`
    conexion.query(sql, (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.get('/pedidocliente', function(req, res, next) {
    let sql = `select s.company_name as cliente, sum(ppc) as ppc, sum(pnc) as pnc, sum(foliares, next) as foliares, sum(semillas) as semillas, sum(estrategicos) as estrategicos, sum(commodities) as commodities from (select c.company_name, o.customer_group_code, ppc*total as 'ppc', pnc*total as 'pnc', foliares*total as 'foliares', semillas*total as 'semillas', estrategicos*total as 'estrategicos', commodities*total as 'commodities' from orders as o join catalog_product_entity as ca on ca.entity_id = o.product_id  join customer as c on c.customer_group_code = o.customer_group_code) as s group by s.customer_group_code`
    conexion.query(sql, (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.get('/pedidovendedor', function(req, res, next) {
    let sql = `select s.nombre_distribuidor as cliente, sum(ppc) as ppc, sum(pnc) as pnc, sum(foliares, next) as foliares, sum(semillas) as semillas, sum(estrategicos) as estrategicos, sum(commodities) as commodities  from (select v.nombre_distribuidor, o.customer_group_code, ppc*total as 'ppc', pnc*total as 'pnc', foliares*total as 'foliares', semillas*total as 'semillas', estrategicos*total as 'estrategicos', commodities*total as 'commodities'  from orders as o join catalog_product_entity as ca on ca.entity_id = o.product_id  join customer as c on c.customer_group_code = o.customer_group_code join customer v on v.customer_group_code = c.vendedor ) as s group by s.nombre_distribuidor `
    conexion.query(sql, (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.get('/pedidolinea', function(req, res, next) {
    let sql = `select sum(ppc+pnc+foliares, next) as total, sum(ppc) as ppc, sum(pnc) as pnc, sum(foliares, next) as foliares, sum(semillas) as semillas, sum(estrategicos) as estrategicos, sum(commodities) as commodities  from (select v.nombre_distribuidor, o.customer_group_code, ppc*total as 'ppc', pnc*total as 'pnc', foliares*total as 'foliares', semillas*total as 'semillas', estrategicos*total as 'estrategicos', commodities*total as 'commodities'  from orders as o join catalog_product_entity as ca on ca.entity_id = o.product_id  join customer as c on c.customer_group_code = o.customer_group_code join customer v on v.customer_group_code = c.vendedor ) as s `
    conexion.query(sql, (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.get('/:id', function(req, res, next) {
    const {id} = req.params
    let sql = 'select * from catalog_product_entity where entity_id = ?'
    conexion.query(sql, [id], (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.get('/order/:id', function(req, res, next) {
    const {id} = req.params
    console.log(id);
    let sql = `SELECT o.entity_id, o.created_at, o.update_at, o.total, o.customer_group_code, o.vendedor, o.product_id, o.price, o.qty, c.name, c.entity_id as entity_product  FROM orders as o join catalog_product_entity as c on o.product_id = c.entity_id where o.customer_group_code = '${id}'`
    conexion.query(sql, [id], (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.get('/busqueda/:id', function(req, res, next) {
    const {id} = req.params
    let sql = `select * from catalog_product_entity where name like '%${id}%' or detail like '%${id}%'`
    conexion.query(sql, [id], (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.get('/buscarcategoria/:id', function(req, res, next) {
    const {id} = req.params
    let sql = "";
    if(id==2){
        sql = `select * from catalog_product_entity where foliares>0`;
    }else if(id==1){
        sql = `select * from catalog_product_entity where ppc>0`;
    }else if(id==0){
        sql = `select * from catalog_product_entity where pnc>0`;
    }
    conexion.query(sql, [id], (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.post('/login', function(req, res, next) {
    const {username, password} = req.body
    console.log('user: ', username, 'password ', password);
    let sql = `select customer_group_code from customer where usr = '${username}' and pass =  '${password}'`
    conexion.query(sql, (err, rows, fields) =>{
        if(err) throw err; 
        else{
            if(rows[0] === undefined){
            res.json(false);   
            }else{
                res.json(rows[0].customer_group_code);
            }
        }
    })
});

route.post('/mailer', async function(req, res, next) {
    const {to, cc, bcc, subject, html} = req.body
    let transporter = await nodemailer.createTransport({
    host: "smtp.office365.com",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
        user: "noreply@disagro.net", // generated ethereal user
        pass: "D1s9gr00720", // generated ethereal password
    },
    });

    let info = await transporter.sendMail({
        from: '"noreply@disagro.net" <noreply@disagro.net>', // sender address
        to: to, // list of receivers
        cc: cc,
        bcc: bcc,
        subject: subject, // Subject line
        text: "", // plain text body
        html: html, // html body
      });

      console.log("Message sent: %s", info.messageId);
      // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>
    
      // Preview only available when sending through an Ethereal account
      console.log("Preview URL: %s", nodemailer.getTestMessageUrl(info));
      res.json("enviado");
});

route.post('/crearoferta', function(req, res, next) {
    const {numoferta, customer_group_code, orden_id} = req.body
    let sql = `insert into oferta (numoferta, customer_group_code, orden_id) values ('${numoferta}', '${customer_group_code}', '${orden_id}');`
    conexion.query(sql, (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.post('/insertoferta', function(req, res, next) {
    console.log("insert");
    const {entrega, customer_group_code, orden_id, tipo, bodega, numref, quantity, destinatario, tipopedido, nota, fecharecepcion, direcciondestino, nompiloto, celpiloto, identificacionpiloto, placacamion} = req.body
    let sql = `insert into oferta (ofertasap, customer_group_code, created_at, orden_id, quantity, numref, Status, destinatario, estado, tipopedido, nota, fecharecepcion, DireccionDestino, NomPiloto, CelPiloto, IdentificacionPiloto, PlacaCamion, entrega, tipo, bodega) values (NULL, ${customer_group_code}, current_timestamp(), ${orden_id}, ${quantity}, '${numref}', 4, '${destinatario}', NULL, ${tipopedido}, '${nota}', '${fecharecepcion}', '${direcciondestino}', '${nompiloto}', '${celpiloto}', '${identificacionpiloto}', '${placacamion}', '${entrega}', '${tipo}', '${bodega}');`
    conexion.query(sql, (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.post('/order', function(req, res, next) {
    const {total, vendedor, customer_group_code, product_id, price, qty} = req.body
    console.log('total: ', total, 'vendedor ', vendedor, ' customer: ', customer_group_code, ' product_id: ', product_id, ' price: ', price, ' qty: ', qty);
    let sql = `insert into orders (total, vendedor, customer_group_code, product_id, price, qty) values ('${total}', '${vendedor}', '${customer_group_code}', '${product_id}', '${price}', '${qty}');`
    conexion.query(sql, (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.put('/:id', function(req, res, next) {
    const {id} = req.params
    const {status} = req.body
    let sql = `update catalog_product_entity set status = '${status}' where entity_id = '${id}'`
    conexion.query(sql, [id], (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.put('/order/:id', function(req, res, next) {
    console.log("entra");
    const {id} = req.params
    const {qty} = req.body
    let sql = `update orders set qty = '${qty}', total=price*qty where entity_id = '${id}'`
    conexion.query(sql, [id], (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.put('/actualizarestado/oferta/:id', function(req, res, next) {
    console.log("actualizar");
    console.log(req.body);
    const {id} = req.params
    const {estado} = req.body
    let sql = `update oferta set estado = '${estado}' where ofertasap = '${id}'`
    conexion.query(sql, [id], (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

route.delete('/order/:id', function(req, res, next) {
    const {id} = req.params
    let sql = 'delete from orders where entity_id = ?'
    conexion.query(sql, [id], (err, rows, fields) =>{
        if(err) throw err;
        else{
            res.json(rows);
        }
    })
});

module.exports = route;